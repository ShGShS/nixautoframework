package spec

import geb.spock.GebReportingSpec
import nix.autoframework.page.StartPage
import nix.autoframework.page.BlogPage

class NIXNavigationSpec extends GebReportingSpec{
    def "Navigate to Start page"(){
        when:
        to StartPage

        and:
        "User navigates to Blog page"()

        then:
        at BlogPage
    }

}

